#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include<bits/stdc++.h>
#include <cstring>
#include <sstream>  

std::string sentence;           //store sentence
std::vector<std::string> words; //vector to store words of sentence
std::string tmp;                //temporary string vector
int number;                     //variable for method
int number1;                    //first number
int number2;                    //second number
int result;                     //result of multiplication, addition, subtraction
float divresult;                //result for division

int convertToint(std::string str){      //method that converts given string to equivalent integer
    if(str=="zero"){
        return number=0;
    }
    else if(str=="one"){
        return number=1;
    }
    else if(str=="two"){
        return number=2;
    }
    else if(str=="three"){
        return number=3;
    }
    else if(str=="four"){
        return number=4;
    }
    else if(str=="five"){
        return number=5;
    }
    else if(str=="six"){
        return number=6;
    }
    else if(str=="seven"){
        return number=7;
    }
    else if(str=="eight"){
        return number=8;
    }
    else if(str=="nine"){
        return number=9;
    }
    else if(str=="ten"){
        return number=10;
    }
    else if(str=="eleven"){
        return number=11;
    }
    else if(str=="twelve"){
        return number=12;
    }
    else if(str=="thirteen"){
        return number=13;
    }
    else if(str=="fourteen"){
        return number=14;
    }
    else if(str=="fiveteen"){
        return number=15;
    }
    else if(str=="sixteen"){
        return number=16;
    }
    else if(str=="seventeen"){
        return number=17;
    }
    else if(str=="eighteen"){
        return number=18;
    }
    else if(str=="nineteen"){
        return number=19;
    }
    else if(str=="twenty"){
        return number=20;
    }
    else{
        return number=21;
    }
}

int main(){
    start:
    std::cout <<"Enter mathematical sentence " << std::endl;
    (std::getline(std::cin, sentence));             //get input
    std::stringstream str_strm(sentence);
    while (str_strm >> tmp){
        std::transform(tmp.begin(), tmp.end(), tmp.begin(), ::tolower);     //lowercase
        words.push_back(tmp);                               //create vector containing words
    }
    number1=convertToint(words[0]);                         //get first number
    number2=convertToint(words[2]);                         //get second number
    if (number1==21 || number2==21){                        //ensure valid input
        std::cout <<"Invalid input " << std::endl;
        tmp.erase();                                    //clear vectors and strings
        words.clear();
        str_strm.str("");       
        goto start;
    }

    
    if (words[1]=="plus"){                                  //perform operations
        result=number1+number2;
        std::cout << "Result is " << result << std::endl;
    }
    else if (words[1]=="minus"){
        result=number1-number2;
        std::cout << "Result is " << result << std::endl;
    }
    else if (words[1]=="times"){
        result=number1*number2;
        std::cout << "Result is " << result << std::endl;
    }
    else if (words[1]=="over"){
        divresult=(float) number1/number2;
        std::cout << "Result is " << divresult << std::endl;
    }
    else{
        std::cout <<"Invalid operation " << std::endl;
        tmp.erase();                                    //clear vectors and strings
        words.clear();
        str_strm.str("");       
        goto start;
    }
}