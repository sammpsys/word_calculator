# Word_calculator

A program that prompts the user for a mathematical sentence where numbers are in the format number operation number and returns the result of said operation. 

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Developers](#developers)

## Requirements

Requires `gcc`.

## Usage

Compile wordcalc.cpp and run. Program will prompt user for input and user must supply three words in the following order: number operation number. Any deviation from the specified format will result in program displaying an error message and reprompting user. Numbers must be in word form (i.e. one, two, three etc.) and the valid range is zero to twenty. Input is not case-sensitive. Operations are addition, subtraction, multiplication and division. Format for operations is plus, minus, times and over. Examples for each are provided. 

<b>Addition</b>

Input: Twenty plus two

Output: Result is 22


<b>Subtraction</b>

Input: Twenty minus two

Output: Result is 18



<b>Multiplication</b>

Input: Eight times two

Output: Result is 16



<b>Division</b>

Input: Eighteen over three

Output: Result is 6

## Author

Sam Nassiri

